$(function(){
	//$("body").fadeOut();
	if($(".datepicker").length){
		$('.datepicker input').datepicker({
			maxViewMode: 0,
			language: "cs"
		});
	}

	$("body .list-group-item-action-remove").on("click", function(){
		$(this).closest('.list-group-item').slideUp(function(){
			$(this).remove();
		});

		return false;
	});
});