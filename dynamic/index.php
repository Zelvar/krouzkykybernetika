<?php
	error_reporting(0);

	$page = strip_tags(trim($_GET["page"]));
	
	if(isset($page) 
		&& 
		!empty($page)
		&&
		(file_exists("./pages/".$page.".php"))){
	}else{
		header("Location: index.php?page=home");
	}

	include "./partials/header.php";
	include "./pages/".$page.".php";
	include "./partials/footer.php";
?>