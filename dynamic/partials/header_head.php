<!DOCTYPE html>
<html lang="cs">
<head>
	<meta charset="UTF-8">
	<title>Kroužky kybernetiky</title>
	
	<!-- VENDOR -->

	<!-- CDN -->
	<link href="https://fonts.googleapis.com/css?family=Merriweather:200,300,400,400i,600,600i,700,700i&subset=latin-ext" rel="stylesheet">

	<!-- LOCAL STYLES -->

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="assets/vendor/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/vendor/css/fontawesome/fontawesome.min.css">
	
	<!-- END VENDOR -->

	<link rel="stylesheet" href="assets/css/styles.css">
</head>