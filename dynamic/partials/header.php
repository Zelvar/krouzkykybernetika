<?php include __DIR__."/header_head.php" ?>
<body>
	<!-- HEADER -->
	<header>
		<div class="stripe">
			<div class="container">
				<div class="row">
					<div class="stripe-text col-12">
						<a href="http://www.vsb.cz/cs" class="college-long">VŠB – Technická univerzita Ostrava</a>
						<!--<a href="http://www.vsb.cz/cs" class="college-short hidden-xs-up">VŠB-TUO</a>-->
						-
						Kroužky kybernetiky
					</div>
				</div>
			</div>
			<div class="stripe-gradient-bar"></div>
		</div>
		
		<div class="white-stripe">
			<div class="container">
				<div class="row">
					<div class="col-lg-3">
						<a href="#" class="header-logo">
							<img src="./assets/images/fei.png" alt="">
						</a>
					</div>
					<div class="col-lg-9">
						<div class="row">
							<div class="col-lg-12 text-right">
								<nav class="header-links">
									<div class="links-primary">
										<a href="#" class="link">Přihlášení</a>
										<a href="#" class="link">Registrace</a>
									</div>
									<div class="links-secondary">
										<a href="#" class="header-link">FEI</a>
										<span class="link-spacer"></span>
										<a href="#" class="header-link">VŠB-TUO</a>
										<span class="link-spacer"></span>
										<a href="#" class="link">Zlepši si techniku</a>
									</div>
								</nav>
							</div>
							<div class="col-lg-12">
								<div class="row justify-content-center">
									<nav class="navbar main-menu navbar-expand-lg">
										<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
											<span class="navbar-toggler-icon"></span>
										</button>
										<div class="collapse navbar-collapse text-right" id="navbarNav">
											<ul class="navbar-nav ml-auto">
												<li class="nav-item">
													<a href="#" class="nav-link">Informace</a>
												</li>
												<li class="nav-item">
													<a href="#" class="nav-link">Aktuality</a>
												</li>
												<li class="nav-item">
													<a href="#" class="nav-link">Sponzoři</a>
												</li>
												<li class="nav-item">
													<a href="#" class="nav-link">Rozvrhy</a>
												</li>
												<li class="nav-item">
													<a href="#" class="nav-link">Galerie</a>
												</li>
												<li class="nav-item">
													<a href="#" class="nav-link">Materiály</a>
												</li>
												<li class="nav-item">
													<a href="#" class="nav-link">Kontakty</a>
												</li>
											</ul>
										</div>
									</nav>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<!-- END HEADER -->