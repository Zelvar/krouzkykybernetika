<div class="main">
	<section class="slider header-image">
		<div id="carouselExampleControls" class="carousel slide h-100" data-ride="carousel">
			<div class="carousel-inner">
				<div class="carousel-item active">
					<img class="d-block w-100" src="https://dummyimage.com/1920x600/000/fff" alt="First slide">
				</div>
				<div class="carousel-item">
					<img class="d-block w-100" src="https://dummyimage.com/1920x600/DDD/fff" alt="Second slide">
				</div>
				<div class="carousel-item">
					<img class="d-block w-100" src="https://dummyimage.com/1920x600/000/fff" alt="Third slide">
				</div>
			</div>
			<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</section>

	<section class="icon-block">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-12 section col-md-border">
					<div class="icon-block-item col-md-border-border">
						<span class="section-icon"><i class="fa fa-code"></i></span>
						<span class="bold big section-link" href="">Programování</span>
						<span class="section-text">
							NXT, EV3, RobotC, LabVew, Scratch, C#
						</span>
					</div>
				</div>
				<div class="col-md-4 col-12 section col-md-border">
					<div class="icon-block-item col-md-border-border">
						<span class="section-icon"><i class="fas fa-robot"></i></span>
						<span class="bold big section-link" href="">Kreativita a robotika</span>
						<span class="section-text">
							Lego Mindstorm EV3, Lego Mindstorm NXT, Robotické hračky
						</span>
					</div>
				</div>
				<div class="col-md-4 col-12 section col-md-border">
					<div class="icon-block-item col-md-border-border">
						<span class="section-icon"><i class="fa fa-magnet"></i></span>
						<span class="bold big section-link" href="">Fyzikální principy</span>
						<span class="section-text">
							Senzory, profesionální měřící systémy
						</span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-12 section col-md-border">
					<div class="icon-block-item col-md-border-border">
						<span class="section-icon"><i class="fas fa-battery-three-quarters"></i></span>
						<span class="bold big section-link" href="">Základy elektrotechniky</span>
						<span class="section-text">
							Boffin, Voltík 1, Voltík 2, Voltík 3
						</span>
					</div>
				</div>
				<div class="col-md-4 col-12 section col-md-border">
					<div class="icon-block-item col-md-border-border">
						<span class="section-icon"><i class="fa fa-bolt"></i></span>
						<span class="bold big section-link" href="">Návrh a vývoj elektroniky</span>
						<span class="section-text">
							Eagle, Leptání, Pájení
						</span>
					</div>
				</div>
				<div class="col-md-4 col-12 section col-md-border">
					<div class="icon-block-item col-md-border-border">
						<span class="section-icon"><i class="fas fa-tachometer-alt"></i></span>
						<span class="bold big section-link" href="">Měřící technika</span>
						<span class="section-text">
							Osciloskopy, Generátory, Zdroje, mikroshop, Senzory
						</span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-12 section col-md-border">
					<div class="icon-block-item col-md-border-border">
						<span class="section-icon"><i class="fas fa-microchip"></i></span>
						<span class="bold big section-link" href="">Programovatelné automaty</span>
						<span class="section-text">
							Plc SIMATIC1200, PLC Logo, TIA-Portal, ControlWeb
						</span>
					</div>
				</div>
				<div class="col-md-4 col-12 section col-md-border">
					<div class="icon-block-item col-md-border-border">
						<span class="section-icon"><i class="fas fa-desktop"></i></span>
						<span class="bold big section-link" href="">Soft skills</span>
						<span class="section-text">
							Prezentace, Jak sepsat životopis, Profesní komunikace, Elektronické nástroje
						</span>
					</div>
				</div>
				<div class="col-md-4 col-12 section col-md-border">
					<div class="icon-block-item col-md-border-border">
						<span class="section-icon"><i class="fas fa-atom"></i></span>
						<span class="bold big section-link" href="">Vývoj a výzkum</span>
						<span class="section-text">
							Brainstorming, Proces vývoje, Technická dokumentace, Publikace, Ochrana duševního vlastnictví
						</span>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>