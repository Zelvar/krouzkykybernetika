<div class="profile-page">
	<section class="slider header-image">
		<img class="d-block w-100" src="https://dummyimage.com/1920x248/000/fff" alt="First slide">
	</section>
	<section class="content-page usercontrol-profile">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-2 col-lg-3 col-md-4 col-12 content-page-nav">
					<?
						//MENU
						include __DIR__."/subpages/partials/sidemenu.html"; //INCLUDE
						//END MENU
					?>
				</div>
				<div class="col-xl-10 col-lg-9 col-md-8 col-12 content-page-content">
					<?
						//STRÁNKA
						if(isset($_PAGE))
							include __DIR__."/subpages/".$_PAGE.".php";  //INCLUDE
						else
							die("Bad layout page.");
						//END STRÁNKA
					?>
				</div>
			</div>
		</div>
	</section>
</div>