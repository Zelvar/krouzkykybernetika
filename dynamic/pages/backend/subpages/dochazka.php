<div class="row">
	<div class="col-xl-12 col-12">
		<? include __DIR__."/partials/breadcrumb.html" ?>
	</div>

	<div class="col-xl-12 col-12">
		<div class="content-page-header">
			<h1 class="page-title">
				Docházka: Jan Novák
			</h1>
			<p>
				Seznam docházky a editace uživatele Jan Novák
			</p>
		</div>
	</div>
</div>

<!-- LAYOUT -->

<div class="row m-5">
	<div class="col-xl-12 p-2">
		<? include __DIR__."/partials/dochazka/records.html" ?>
	</div>
	<? include __DIR__."/partials/dochazka/pagination.html" ?>
</div>

<div class="row mt-2">
	<div class="col-xl-12 col-12 border-top">
		<? include __DIR__."/partials/dochazka/form.html" ?>
	</div>
</div>